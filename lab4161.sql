USE company;
CREATE TABLE employees_first (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255),
  `lastname` VARCHAR(255),
  `title` VARCHAR(255),
  `age` INT(3),
  `salary` DECIMAL,
  PRIMARY KEY (`id`)
);

employees_first